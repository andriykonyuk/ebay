import json
from pprint import pprint

import requests
from bs4 import BeautifulSoup
from setting import EBAY_LOGIN, EBAY_PASS, EBAY_USER_ID
from math import ceil
import aiohttp
import asyncio


class EbayParser:
    def __init__(self):
        self.login = EBAY_LOGIN
        self.password = EBAY_PASS
        self.url_login = 'https://www.ebay.com/signin/s'
        self.url_to_parse = 'https://www.ebay.com/usr/' + EBAY_USER_ID
        self.feedback_url = ''

    def login_to_ebay(self):
        session = requests.session()
        page = session.get(self.url_login)
        page_cookies = page.cookies.get_dict()
        soup = BeautifulSoup(page.content, 'html.parser')
        form = soup.find('form', attrs={"id": "SignInForm"})
        url_to_login = form['action']
        div = form.find('div', attrs={"class": "signin-view-container"})
        inputs = div.find_all('input')
        payloads = {}
        for item in inputs:
            payloads[item['name']] = item.get('value', "")

        payloads['userid'] = EBAY_LOGIN
        payloads['pass'] = EBAY_PASS
        payloads = json.dumps(payloads)
        headers = {'Content-Type': 'text/plain'}

        new_page = session.post(url_to_login, data=payloads, headers=headers, cookies=page_cookies)
        page_auth = BeautifulSoup(new_page.content, 'html.parser')
        page_title = page_auth.find('title').text
        if page_title == 'Electronics, Cars, Fashion, Collectibles, Coupons and More | eBay':
            print('Login was successful')
        else:
            print('Error when trying to login')

        return page_auth

    def get_info_profile(self):
        page = requests.get(self.url_to_parse)
        if page.status_code == 200:
            html = page.content
            soup = BeautifulSoup(html, 'html.parser')

            score = soup.find_all('div', attrs={"class": "score"})
            score_positive = score[0].findChildren('span', attrs={"class": "num"})[0].text
            score_neutral = score[1].findChildren('span', attrs={"class": "num"})[0].text
            score_negative = score[2].findChildren('span', attrs={"class": "num"})[0].text

            print(f'Positive: {score_positive} | Neutral: {score_neutral} | Negative: {score_negative}')

            self.feedback_url = soup.find('span', attrs={"class": "all_fb fr"}).findChild()['href']

            if self.feedback_url is None:
                self.feedback_url = soup.find('a', attrs={'title': 'See all feedback'})['href']

    def get_requests_count(self, items):
        html = requests.get(self.feedback_url + f'&interval=180&items={items}')
        if html.status_code == 200:
            page = html.content
            soup = BeautifulSoup(page, 'html.parser')
            temp_str = soup.find('td', attrs={'class': 'FeedBackStatusLine'}).text
            number_of_comments = [int(s) for s in temp_str.split() if s.isdigit()][0]
            will_be_pages = ceil(number_of_comments / items)
            return will_be_pages

    def get_info_comments(self):
        count_page = self.get_requests_count(25)
        list_of_page = [self.feedback_url + f"&which=all&interval=180&page={i}" for i in range(1, count_page + 1)]
        tasks = [self.get_content(url) for url in list_of_page]
        loop = asyncio.get_event_loop()
        loop.run_until_complete(asyncio.wait(tasks))

    @classmethod
    async def fetch(cls, session, url):
        async with session.get(url) as response:
            if response.status == 200:
                return await response.text()

    @classmethod
    async def get_content(cls, url):
        async with aiohttp.ClientSession() as session:
            page = await cls.fetch(session, url)
            soup = BeautifulSoup(page, 'html.parser')

            removals = soup.find_all('td', attrs={'id': 'info90daysMsg'})
            for match in removals:
                match.parent.extract()

            removals = soup.find_all('tr')
            for match in removals:
                if len(match) < 4:
                    match.extract()

            table = soup.find('table', attrs={"class": 'FbOuterYukon'})
            table_tr = table.find_all('tr')[1:]
            i, j = 0, 2
            for _ in range(len(table_tr) // 2):
                comment_tr = table_tr[i:j]
                try:
                    rate = comment_tr[0].find_all('td')[0].findChild()['src']
                except Exception:
                    rate = "Positive"

                if 'iconPos' in rate:
                    rating = '+'
                elif 'iconNeu' in rate:
                    rating = '-'
                elif 'iconNeg' in rate:
                    rating = '+-'
                else:
                    rating = ''

                comment = comment_tr[0].find_all('td')[1].text
                try:
                    price = float(comment_tr[1].find_all('td')[2].text.replace("US", '').replace('$', '').replace('EUR',
                                                                                                                  '').strip())
                except Exception:
                    price = 0
                print(rating, comment, price)
                i, j = i + 2, j + 2


if __name__ == '__main__':
    ebay = EbayParser()

    # get_info_profile must always be first, because from this method we get feedback url
    ebay.get_info_profile()

    ebay.get_info_comments()

    ebay.login_to_ebay()
